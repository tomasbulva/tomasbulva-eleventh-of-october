import {createContext} from 'react';
import {GlobalStateIntf} from '../state';

export const StateContext = createContext<Partial<GlobalStateIntf>>({});
