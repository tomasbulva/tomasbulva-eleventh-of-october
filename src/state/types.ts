export type OrderbookRow = [number, number, number];
export type OrderbookRows = OrderbookRow[];
export type OrderbookCalcRows = [number, number][];
export type Spread = {value: number; percent: number};

export type OrderbookMessage = {
    asks: OrderbookRows;
    bids: OrderbookRows;
    spread: Spread;
    highestTotal: number;
};

export type EventMessage = {
    feed: string;
    product_id: string;
    bids: OrderbookCalcRows;
    asks: OrderbookCalcRows;
    numLevels?: number;
    event?: 'subscribed' | 'unsubscribed' | undefined;
};
