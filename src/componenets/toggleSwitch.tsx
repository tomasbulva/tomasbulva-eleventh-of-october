import React, {useContext} from 'react';
import {DispatchContext} from '../context/dispatchContext';
import {StateContext} from '../context/stateContext';
import {getContractMessage} from '../utils/api';
import {getOtherContract} from '../utils/constans';

export default function ToggleSwitch() {
    const dispatch = useContext(DispatchContext);
    const {socket, currentContract} = useContext(StateContext);

    const handleToggle = () => {
        socket.send(getContractMessage(currentContract, false));
        dispatch({type: 'clearOrderBook'});
        socket.send(
            getContractMessage(getOtherContract(currentContract), true),
        );
        dispatch({type: 'contractToggle'});

        //dispatch({type: 'closeSocket'});
        // dispatch({type: 'clearOrderBook'});
        // const interval = setInterval(() => {
        //     console.log('socket.readyState', socket.readyState);
        //     if (socket.readyState === socket.CLOSED) {
        //         clearInterval(interval);
        //         dispatch({type: 'contractToggle'});
        //         dispatch({type: 'openSocket'});
        //     }
        // }, 100);
    };
    return (
        <button className="toggle-switch" onClick={handleToggle}>
            Toggle Feed
        </button>
    );
}
