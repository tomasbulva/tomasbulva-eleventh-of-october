import { HeaderLine } from './componenets/headerLine';
import React, {useState} from 'react';
import ToggleSwitch from './componenets/toggleSwitch/toggleSwitch';
import './styles.scss';

export const OrderBook = () => {
    const [isToggled, setIsToggled] = useState(false);

    return (
        <main>
            <section className="orderbook">
                <aside className="green">
                    <header>
                        <span>Order Book</span>
                    </header>
                    <HeaderLine />
                    <div className="result-line">
                        <div className="total">1,200</div>
                        <div className="size">1,200</div>
                        <div className="total">34,062.50</div>
                        <div className="background" />
                    </div>
                </aside>
                <aside className="red">
                    <header>
                        <span>Spread 17.0 (0.05%)</span>
                    </header>
                    <HeaderLine />
                    <div className="result-line">
                        <div className="total">1,200</div>
                        <div className="size">1,200</div>
                        <div className="total">34,062.50</div>
                        <div className="background" />
                    </div>
                </aside>
            </section>
            <footer><ToggleSwitch isToggled={isToggled} setIsToggled={setIsToggled} /></footer>
        </main>
    );
};
