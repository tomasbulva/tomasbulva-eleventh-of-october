import React, { useReducer, useState } from 'react';

import RedSide from './componenets/redSide';
import GreenSide from './componenets/greenSide';
import ToggleSwitch from './componenets/toggleSwitch';

import { defState, reducer } from './state';

import './styles.scss';

export default function OrderBook() {
    const [state, dispatch] = useReducer(reducer, defState);
    const [isToggled, setIsToggled] = useState(false);

    return (
        <main>
            <section className="orderbook">
                <GreenSide />
                <RedSide />
            </section>
            <footer>
                <ToggleSwitch
                    isToggled={isToggled}
                    setIsToggled={setIsToggled}
                />
            </footer>
        </main>
    );
};
