import React, {useEffect, useReducer, useState} from 'react';

import RedSide from './componenets/redSide';
import GreenSide from './componenets/greenSide';
import ToggleSwitch from './componenets/toggleSwitch';

import {StateContext} from './context/stateContext';
import {DispatchContext} from './context/dispatchContext';

import {defState, reducer} from './state';

import './styles.scss';
import useBrowser from './hooks/useBrowser';
import useOrderbook from './hooks/useOrderbook';
import useHardwarePerf from './hooks/useHardwarePerf';

export default function OrderBook() {
    const [state, dispatch] = useReducer(reducer, defState);
    const [isToggled, setIsToggled] = useState(false);

    useHardwarePerf();
    useOrderbook({state, dispatch});
    useBrowser({state, dispatch});

    useEffect(() => {
        // start the application
        dispatch({type: 'openSocket'});
    }, []);

    return (
        <DispatchContext.Provider value={dispatch}>
            <StateContext.Provider value={state}>
                <main>
                    <section className="orderbook">
                        <GreenSide />
                        <RedSide />
                    </section>
                    <footer>
                        <ToggleSwitch
                            isToggled={isToggled}
                            setIsToggled={setIsToggled}
                        />
                    </footer>
                </main>
            </StateContext.Provider>
        </DispatchContext.Provider>
    );
}
