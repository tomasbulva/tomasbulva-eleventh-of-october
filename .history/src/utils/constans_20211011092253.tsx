export const API = 'wss://www.cryptofacilities.com/ws/v1';
export const CONTRACT = {
    'XBT-USD': 'PI_XBTUSD',
    'ETH-USD': 'PI_ETHUSD',
};
export const CONTRACT_MAP = {
    PI_XBTUSD: 'XBT-USD',
    PI_ETHUSD: 'ETH-USD',
};

export const getContractLabel = (contractVal: string): string => {
    return CONTRACT_MAP[contractVal];
};

export const getOtherContract = (contractVal: string): string => {
    if (contractVal === CONTRACT['XBT-USD']) {
        return CONTRACT['ETH-USD'];
    }
    return CONTRACT['XBT-USD'];
};

export const locale = 'en-us';
