import {
    EventMessage,
    OrderbookCalcRows,
    OrderbookMessage,
    OrderbookRows,
    Spread,
} from '../state/types';

// class OrderBookController {
//     bids: OrderbookRows = [];
//     asks: OrderbookRows = [];
//     highestTotal = 0;
//     spread: Spread = {value: 0, percent: 0};

//     input(message: MessageEvent['data']): OrderbookMessage {
//         const messageData = JSON.parse(message) as EventMessage;

//         if (messageData.event === 'unsubscribed') {
//             this.bids = [];
//             this.asks = [];
//             this.highestTotal = 0;
//             this.spread = {value: 0, percent: 0};
//             return this;
//         }

//         if (messageData.asks) {
//             this.asks = this.process(
//                 messageData.asks,
//                 Object.fromEntries(this.asks),
//                 'ask',
//             );
//         }
//         if (messageData.bids) {
//             this.bids = this.process(
//                 messageData.bids,
//                 Object.fromEntries(this.bids),
//                 'bid',
//             );
//         }

//         const highestBid = this.bids.length
//             ? this.bids[this.bids.length - 1][2]
//             : 0;
//         const highestAsk = this.asks.length
//             ? this.asks[this.asks.length - 1][2]
//             : 0;

//         const firstBid = this.bids.length ? this.bids[0][0] : 0;
//         const firstAsk = this.asks.length ? this.asks[0][0] : 0;
//         console.log('firstBid', this.bids);
//         console.log('firstAsk', this.asks);
//         this.highestTotal = Math.max(highestBid, highestAsk);

//         const spread = {
//             value: Number(Math.abs(firstBid - firstAsk).toFixed(1)),
//             percent: Number(
//                 Math.abs(100 - (firstBid / firstAsk) * 100).toFixed(2),
//             ),
//         };
//         this.spread = spread;
//         return this;
//     }

//     process(
//         incomingDeals: OrderbookCalcRows,
//         currentDeals: Record<string, number>,
//         type: string,
//     ): OrderbookRows {
//         if (!Array.isArray(incomingDeals)) {
//             throw new Error(`Wrong data: ${incomingDeals}`);
//         }

//         // turn to object to guarantie deals unique by price
//         incomingDeals.forEach((bid) => {
//             const [price, size] = bid;
//             if (size === 0) {
//                 delete currentDeals[price];
//                 return;
//             }
//             currentDeals[price] = size;
//         });

//         // turn object back into array of price,size
//         const dealsArray = Object.entries(currentDeals);

//         dealsArray.sort((a, b) => {
//             return Number(b[0]) - Number(a[0]);
//         });

//         // truncate deals array
//         if (dealsArray.length >= 26) {
//             dealsArray.length = 25;
//         }

//         let dealsTotal = 0;
//         const bidsWithTotal: OrderbookRows = dealsArray.map((bid) => {
//             const [price, size] = bid;
//             const numericPrice = Number(price);
//             dealsTotal += size;
//             console.log(type, 'dealsTotal', dealsTotal);

//             return [numericPrice, size, dealsTotal];
//         });

//         return bidsWithTotal;
//     }
// }

class OrderBookController {
    bids: OrderbookRows = [];
    asks: OrderbookRows = [];
    highestTotal = 0;
    spread: Spread = {value: 0, percent: 0};

    input(message: MessageEvent['data']): OrderbookMessage {
        const messageData = JSON.parse(message) as EventMessage;

        if (messageData.event === 'unsubscribed') {
            this.bids = [];
            this.asks = [];
            this.highestTotal = 0;
            this.spread = {value: 0, percent: 0};
            return this;
        }

        if (messageData.asks) {
            const nextAsks = this.processAsks(messageData.asks);
            this.asks = nextAsks;
        }
        if (messageData.bids) {
            const nextBids = this.processBids(messageData.bids);
            this.bids = nextBids;
        }

        const highestBid = this.bids.length
            ? this.bids[this.bids.length - 1][2]
            : 0;
        const highestAsk = this.asks.length
            ? this.asks[this.asks.length - 1][2]
            : 0;

        const firstBid = this.bids.length ? this.bids[0][0] : 0;
        const firstAsk = this.asks.length ? this.asks[0][0] : 0;

        this.highestTotal = Math.max(highestBid, highestAsk);

        const spread = {
            value: Number(Math.abs(firstBid - firstAsk).toFixed(1)),
            percent: Number(
                Math.abs(100 - (firstBid / firstAsk) * 100).toFixed(2),
            ),
        };
        this.spread = spread;
        return this;
    }

    processBids(incomingBids: OrderbookCalcRows): OrderbookRows {
        if (!Array.isArray(incomingBids)) {
            throw new Error(`Wrong data: ${incomingBids}`);
        }
        // turn to object to guarantie deals unique by price
        const nextBids: Record<string, number> = Object.fromEntries(this.bids);

        incomingBids.forEach((bid) => {
            const [price, size] = bid;
            if (size === 0) {
                delete nextBids[price];
                return;
            }
            nextBids[price] = size;
        });

        // turn object back into array of price,size
        const asArray = Object.entries(nextBids);

        asArray.sort((a, b) => {
            return Number(b[0]) - Number(a[0]);
        });

        // cull extra bids before after sorting
        if (asArray.length >= 26) {
            asArray.length = 25;
        }

        let prevTotal = 0;
        const bidsWithTotal: OrderbookRows = asArray.map((bid, idx) => {
            const [price, size] = bid;
            const numericPrice = Number(price);
            prevTotal += size;

            return [numericPrice, size, prevTotal];
        });

        return bidsWithTotal;
    }

    processAsks(incomingAsks: OrderbookCalcRows): OrderbookRows {
        if (!Array.isArray(incomingAsks)) {
            throw new Error(`Wrong data: ${incomingAsks}`);
        }
        const nextAsks: Record<string, number> = Object.fromEntries(this.asks);

        incomingAsks.forEach((ask) => {
            const [price, size] = ask;
            if (size === 0) {
                delete nextAsks[price];
                return;
            }
            nextAsks[price] = size;
        });

        // turn object back into array of price,size
        const asArray = Object.entries(nextAsks);

        asArray.sort((a, b) => {
            return Number(a[0]) - Number(b[0]);
        });

        // cull extra asks before after sorting
        if (asArray.length >= 26) {
            asArray.length = 25;
        }

        let prevTotal = 0;
        const asksWithTotal: OrderbookRows = asArray.map((ask, idx) => {
            const [price, size] = ask;
            const numericPrice = Number(price);
            prevTotal += size;

            return [numericPrice, size, prevTotal];
        });
        return asksWithTotal;
    }
}

const controller = new OrderBookController();
onmessage = (data: MessageEvent['data']) => {
    const result = controller.input(data.data);
    self.postMessage(result);
};
