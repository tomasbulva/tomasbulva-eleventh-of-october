interface createInterface {
    callback: () => void;
    interval: number;
}
export default {
    intervals: new Set(),

    create({callback, interval}: createInterface) {
        const newInterval = setInterval(callback, interval);
        this.intervals.add(newInterval);
        return newInterval;
    },

    // clear a single interval
    clear(id) {
        this.intervals.delete(id);
        return clearInterval(id);
    },

    // clear all intervals
    clearAll() {
        for (var id of this.intervals) {
            this.clear(id);
        }
    },
};
