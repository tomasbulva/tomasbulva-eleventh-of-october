export const getContractMessage = (contract: string) => {
    return `{"event":"subscribe","feed":"book_ui_1","product_ids":["${contract}"]}`;
};

export const worker = new Worker('./worker.ts');