export const API = 'wss://www.cryptofacilities.com/ws/v1';
export const CONTRACT = {
    'XBT-USD': 'PI_XBTUSD',
    'ETH-USD': 'PI_ETHUSD',
};

const getContractLabel = (contractVal) => {
    return Object.keys(CONTRACT).filter((label) => CONTRACT[label] === contractVal).pop();
}
