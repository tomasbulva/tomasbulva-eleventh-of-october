export const getContractMessage = (contract: string, subscribe: boolean = true) => {
    const type = subscribe ? 'subscribe' : 'unsubscribe';
    return `{"event":"${type}","feed":"book_ui_1","product_ids":["${contract}"]}`;
};

export const worker = new Worker('./worker.ts');