export const API = 'wss://www.cryptofacilities.com/ws/v1';
export const CNT = {
  'XBT-USD': 'PI_XBTUSD',
  'ETH-USD': 'PI_ETHUSD',
};