export const getContractMessage = (contract: string, subscribe = true) => {
    const type = subscribe ? 'subscribe' : 'unsubscribe';
    return `{"event":"${type}","feed":"book_ui_1","product_ids":["${contract}"]}`;
};

export const worker = new Worker(new URL('./worker.ts', import.meta.url));


