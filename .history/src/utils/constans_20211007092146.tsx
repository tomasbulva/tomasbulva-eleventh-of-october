export const API = 'wss://www.cryptofacilities.com/ws/v1';
export const CONTRACT = {
    'XBT-USD': 'PI_XBTUSD',
    'ETH-USD': 'PI_ETHUSD',
};

export const getContractLabel = (contractVal) => {
    const result = Object.keys(CONTRACT).filter((label) => CONTRACT[label] === contractVal);
    console.log('getContractLabel', result);
    return result.pop();
}
