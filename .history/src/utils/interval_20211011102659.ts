interface createInterface {
    callback: () => void;
    interval: number;
}

export default {
    intervals: new Set(),

    create({callback, interval}: createInterface) {
        const newInterval = setInterval(callback, interval);
        this.intervals.add(newInterval);
        return newInterval;
    },

    clear(id: number) {
        this.intervals.delete(id);
        return clearInterval(id);
    },

    clearAll() {
        for (const id of this.intervals) {
            this.clear(id);
        }
    },
};
