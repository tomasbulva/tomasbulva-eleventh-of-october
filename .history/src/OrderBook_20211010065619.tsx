import React, {useReducer, useState} from 'react';

import RedSide from './componenets/redSide';
import GreenSide from './componenets/greenSide';
import ToggleSwitch from './componenets/toggleSwitch';

import {StateContext} from './context/stateContext';
import {DispatchContext} from './context/dispatchContext';

import {defState, reducer} from './state';

import './styles.scss';
import useBrowser from './hooks/useBrowser';
import useSocket from './hooks/useSocket';
import useOrderbook from './hooks/useOrderbook';
import useHardwarePerf from 'hooks/useHardwarePerf';

export default function OrderBook() {
    const [state, dispatch] = useReducer(reducer, defState);
    const [isToggled, setIsToggled] = useState(false);

    const {closeSocket, restartSocket} = useSocket({state, dispatch});
    const [bufferDumpIntervalSize] = useHardwarePerf();
    useBrowser({state, dispatch, closeSocket, restartSocket});
    useOrderbook({state, dispatch, bufferDumpIntervalSize});

    return (
        <DispatchContext.Provider value={dispatch}>
            <StateContext.Provider value={state}>
                <main>
                    <section className="orderbook">
                        <GreenSide />
                        <RedSide />
                    </section>
                    <footer>
                        <ToggleSwitch
                            isToggled={isToggled}
                            setIsToggled={setIsToggled}
                        />
                    </footer>
                </main>
            </StateContext.Provider>
        </DispatchContext.Provider>
    );
}
