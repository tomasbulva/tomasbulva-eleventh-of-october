import React from "react";
import HeaderLine from "./headerLine";
import OrderbookHeaderRed from "./orderbookHeaderRed";
import ResultLine from "./resultLine";

export default function RedSide({}) {
    return (
        <aside className="red">
        <OrderbookHeaderRed />
        <HeaderLine />
        <ResultLine />
        </aside>
    );
}
  