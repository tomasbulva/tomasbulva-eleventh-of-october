import React, {useContext} from 'react';

import HeaderLine from './headerLine';
import OrderbookHeaderGreen from './orderbookHeaderGreen';
import ResultLine from './resultLine';
import {StateContext} from '../context/stateContext';
import {OrderbookRow} from '../state/types';
//bid
export default function GreenSide() {
    const {orderbook} = useContext(StateContext);
    return (
        <aside className="green">
            <OrderbookHeaderGreen />
            <HeaderLine />
            {orderbook?.bids?.map((bid: OrderbookRow) => (
                <ResultLine trade={bid.reverse()} key={JSON.stringify(bid)} />
            ))}
        </aside>
    );
}
