import React, { useContext, useEffect, useState } from 'react';
import { StateContext } from '../context/stateContext';
export default function ResultLine({ask}) {
    const [price, size, total] = ask;
    const { orderbook } = useContext(StateContext);
    const [width, setWidth] = useState(0);
    useEffect(() => {
        const calcWidth = (total / orderbook.highestTotal) * 100;
        setWidth(calcWidth);
    }, [total, orderbook.highestTotal]);
    return (
        <div className="result-line">
            <div className="price">{price}</div>
            <div className="size">{size}</div>
            <div className="total">{total}</div>
            <div className="background" style={{width: `${width}%`}} />
        </div>
    );
}
