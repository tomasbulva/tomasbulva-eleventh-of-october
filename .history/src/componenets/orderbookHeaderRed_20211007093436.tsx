import React, {useContext} from 'react';
import {StateContext} from '../context/stateContext';

export default function OrderbookHeaderRed() {
    const {
        orderbook: {
            spread: {value, percent},
        },
    } = useContext(StateContext);
    return (
        <header>
            Spread {value} ({percent}%)
        </header>
    );
}
