import React from 'react';

export default function HeaderLine() {
    return (
        <div className="header-line">
            <div className="total">TOTAL</div>
            <div className="size">SIZE</div>
            <div className="total">PRICE</div>
        </div>
    );
}
