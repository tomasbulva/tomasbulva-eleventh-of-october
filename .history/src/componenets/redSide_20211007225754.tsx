import React from 'react';

import HeaderLine from './headerLine';
import OrderbookHeaderRed from './orderbookHeaderRed';
import ResultLine from './resultLine';
import {StateContext} from '../context/stateContext';
//ask
export default function RedSide() {
    const {orderbook} = useContext(StateContext);

    return (
        <aside className="red">
            <OrderbookHeaderRed />
            <HeaderLine />
            {orderbook.asks.map((ask) => <ResultLine trade={ask} />)}
        </aside>
    );
}
