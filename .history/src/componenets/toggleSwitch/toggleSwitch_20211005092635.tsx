import React, {useState} from 'react';
import './toggleSwitch.scss';

const ToggleSwitch = ({isToggled, setIsToggled}) => {
    const onToggle = () => setIsToggled(!isToggled);
    return (
        <label className="toggle-switch">
            <input type="checkbox" checked={isToggled} onChange={onToggle} />
            <span className="switch" />
        </label>
    );
};

export default ToggleSwitch;
