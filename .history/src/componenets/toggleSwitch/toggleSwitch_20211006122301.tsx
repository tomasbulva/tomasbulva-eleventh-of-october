import React from 'react';

interface Props {
    isToggled: boolean;
    setIsToggled: () => void;
}

const ToggleSwitch = ({isToggled, setIsToggled}) => {
    const onToggle = () => setIsToggled(!isToggled);
    return (
        <button className="toggle-switch" onClick={onToggle}>
            Toggle Feed
        </button>
    );
};

export default ToggleSwitch;
