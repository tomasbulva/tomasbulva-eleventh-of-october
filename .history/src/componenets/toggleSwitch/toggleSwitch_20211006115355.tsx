import React from 'react';
import './toggleSwitch.scss';

const ToggleSwitch = ({isToggled, setIsToggled}) => {
    const onToggle = () => setIsToggled(!isToggled);
    return (
        <button className="toggle-switch" onClick={onToggle} />
    );
};

export default ToggleSwitch;
