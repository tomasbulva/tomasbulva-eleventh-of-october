import React from 'react';

const ToggleSwitch = ({isToggled, setIsToggled}) => {
    const onToggle = () => setIsToggled(!isToggled);
    return (
        <button className="toggle-switch" onClick={onToggle} />
    );
};

export default ToggleSwitch;
