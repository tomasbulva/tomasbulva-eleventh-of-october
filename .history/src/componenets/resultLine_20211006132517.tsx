import React from 'react';

export default function ResultLine() {
    return (
        <div className="result-line">
            <div className="total">1,200</div>
            <div className="size">1,200</div>
            <div className="total">34,062.50</div>
            <div className="background" />
        </div>
    );
}
