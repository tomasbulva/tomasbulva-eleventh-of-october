import React from 'react';

interface Props {
    type: string;
}
export default function HeaderLine({type}: Props) {
    if (type === 'ask') {
        return (
            <div className="header-line">
                <div className="total">TOTAL</div>
                <div className="size">SIZE</div>
                <div className="total">PRICE</div>
            </div>
        );
    }

    return (
        <div className="header-line">
            <div className="total">TOTAL</div>
            <div className="size">SIZE</div>
            <div className="total">PRICE</div>
        </div>
    );
}
