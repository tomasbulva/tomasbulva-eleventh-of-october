import React, {useContext} from 'react';

import HeaderLine from './headerLine';
import OrderbookHeaderRed from './orderbookHeaderRed';
import ResultLine from './resultLine';
import {StateContext} from '../context/stateContext';
import {OrderbookRow} from '../state/types';
//ask
export default function RedSide() {
    const {orderbook} = useContext(StateContext);
    console.log('orderbook', orderbook);
    return (
        <aside className="red">
            <OrderbookHeaderRed />
            <HeaderLine />
            {orderbook?.asks?.map((ask: OrderbookRow) => (
                <ResultLine trade={ask} key={JSON.stringify(ask)} />
            ))}
        </aside>
    );
}
