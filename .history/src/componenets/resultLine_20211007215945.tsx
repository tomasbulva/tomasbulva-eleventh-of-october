import React, { useEffect, useState } from 'react';

export default function ResultLine({ask}) {
    const [price, size, total] = ask;
    const [width, setWidth] = useState(0);
    useEffect(() => {
        const calcWidth = (total / highest) * 100;
        setWidth(calcWidth);
    }, [total, highest]);
    return (
        <div className="result-line">
            <div className="price">{price}</div>
            <div className="size">{size}</div>
            <div className="total">{total}</div>
            <div className="background" style={{width: `${width}%`}} />
        </div>
    );
}
