import React from 'react';

export default function ResultLine({ask}) {
    const [price, size, total] = ask;
    return (
        <div className="result-line">
            <div className="price">{price}</div>
            <div className="size">{size}</div>
            <div className="total">{total}</div>
            <div className="background" />
        </div>
    );
}
