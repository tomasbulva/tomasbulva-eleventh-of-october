import React, { useContext } from 'react';
import { StateContext } from '../context/stateContext';
import { getContractLabel } from '../utils/constans';

export default function OrderbookHeaderGreen() {
    const { currentContract } = useContext(StateContext);
    return <header>Order Book ({getContractLabel(currentContract)})</header>;
}
