import React, {useContext} from 'react';
import {DispatchContext} from '../context/dispatchContext';
import {StateContext} from '../context/stateContext';

export default function ToggleSwitch() {
    const dispatch = useContext(DispatchContext);
    const {socket} = useContext(StateContext);

    const handleToggle = () => {
        dispatch({type: 'closeSocket'});
        dispatch({type: 'clearOrderBook'});
        const interval = setInterval(() => {
            console.log('socket.readyState', socket.readyState);
            if (socket.readyState === socket.CLOSED) {
                clearInterval(interval);
                dispatch({type: 'contractToggle'});
                dispatch({type: 'openSocket'});
            }
        }, 100);
    };
    return (
        <button className="toggle-switch" onClick={handleToggle}>
            Toggle Feed
        </button>
    );
}
