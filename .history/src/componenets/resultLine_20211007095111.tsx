import React from 'react';

export default function ResultLine({ask}) {
    const [price, size, total] = ask;
    return (
        <div className="result-line">
            <div className="price">{ask[]}</div>
            <div className="size">1,200</div>
            <div className="total">34,062.50</div>
            <div className="background" />
        </div>
    );
}
