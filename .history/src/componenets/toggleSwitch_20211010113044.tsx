import React, {useContext} from 'react';
import {DispatchContext} from '../context/dispatchContext';

export default function ToggleSwitch() {
    const dispatch = useContext(DispatchContext);
    const handleToggle = () => {
        dispatch({type: 'closeSocket'});
        dispatch({type: 'contractToggle'});
        dispatch({type: 'clearOrderBook'});
        dispatch({type: 'openSocket'});
    };
    return (
        <button className="toggle-switch" onClick={handleToggle}>
            Toggle Feed
        </button>
    );
}
