import React, {useContext} from 'react';
import {DispatchContext} from '../context/dispatchContext';
import {StateContext} from '../context/stateContext';

export default function ToggleSwitch() {
    const dispatch = useContext(DispatchContext);
    const {} = useContext(DispatchContext);
    const handleToggle = () => {
        dispatch({type: 'closeSocket'});
        dispatch({type: 'clearOrderBook'});

        dispatch({type: 'contractToggle'});
        dispatch({type: 'openSocket'});
    };
    return (
        <button className="toggle-switch" onClick={handleToggle}>
            Toggle Feed
        </button>
    );
}
