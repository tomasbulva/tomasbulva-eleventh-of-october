import React, {useContext, useEffect, useState} from 'react';
import {OrderbookRow} from '../state/types';
import {StateContext} from '../context/stateContext';
import {LOCALE} from '../utils/constans';

interface Props {
    trade: OrderbookRow;
    type?: string;
}

export default function ResultLine({trade, type}: Props) {
    const [price, size, total] = trade;
    const {orderbook} = useContext(StateContext);
    const [width, setWidth] = useState(0);
    useEffect(() => {
        const calcWidth = ((total / orderbook.highestTotal) * 100) / 4;
        setWidth(calcWidth);
    }, [total, orderbook.highestTotal]);

    if (type === 'ask')
        return (
            <div className="result-line">
                <div className="price">{price.toLocaleString(LOCALE)}</div>
                <div className="size">{size.toLocaleString(LOCALE)}</div>
                <div className="total">{total.toLocaleString(LOCALE)}</div>
                <div className="background" style={{width: `${width}%`}} />
            </div>
        );
}
