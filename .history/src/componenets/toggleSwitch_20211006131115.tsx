import React from 'react';

interface Props {
    isToggled: boolean;
    setIsToggled: (isToggled: boolean) => void;
}

const ToggleSwitch = ({isToggled, setIsToggled}:Props) => {
    const onToggle = () => setIsToggled(!isToggled);
    return (
        <button className="toggle-switch" onClick={onToggle}>
            Toggle Feed
        </button>
    );
};

export default ToggleSwitch;
