import React from 'react';

import HeaderLine from './headerLine';
import OrderbookHeaderGreen from './orderbookHeaderGreen';
import ResultLine from './resultLine';


export default function GreenSide({}) {
    const {orderbook} = useContext(StateContext);
    return (
        <aside className="green">
            <OrderbookHeaderGreen />
            <HeaderLine />
            <ResultLine />
        </aside>
    );
}
