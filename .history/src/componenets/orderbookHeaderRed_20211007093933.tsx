import React, {useContext} from 'react';
import {StateContext} from '../context/stateContext';

export default function OrderbookHeaderRed() {
    const { orderbook } = useContext(StateContext);
    // : {
    //     spread: {value, percent},
    // },
    return (
        <header>
            Spread {value} ({percent}%)
        </header>
    );
}
