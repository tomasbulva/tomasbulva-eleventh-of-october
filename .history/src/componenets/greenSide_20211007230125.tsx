import React, {useContext} from 'react';

import HeaderLine from './headerLine';
import OrderbookHeaderGreen from './orderbookHeaderGreen';
import ResultLine from './resultLine';
import {StateContext} from '../context/stateContext';
//bid
export default function GreenSide() {
    const {orderbook} = useContext(StateContext);
    return (
        <aside className="green">
            <OrderbookHeaderGreen />
            <HeaderLine />
            {orderbook?.bids?.map((bid) => <ResultLine trade={bid} />)}
        </aside>
    );
}
