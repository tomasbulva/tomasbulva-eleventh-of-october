import React, {useContext} from 'react';
import {DispatchContext} from '../context/dispatchContext';

export default function ToggleSwitch() {
    const dispatch = useContext(DispatchContext);
    return (
        <button
            className="toggle-switch"
            onClick={() => dispatch({type: 'contractToggle'})}>
            Toggle Feed
        </button>
    );
}
