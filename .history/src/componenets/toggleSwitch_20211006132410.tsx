import React from 'react';

interface Props {
    isToggled: boolean;
    setIsToggled: (isToggled: boolean) => void;
}

export default function ToggleSwitch({isToggled, setIsToggled}:Props) {
    const onToggle = () => setIsToggled(!isToggled);
    return (
        <button className="toggle-switch" onClick={onToggle}>
            Toggle Feed
        </button>
    );
};
