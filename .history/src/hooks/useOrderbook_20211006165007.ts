import {useEffect} from 'react';
import {getContractMessage} from '../utils/api';

interface Props {
    state: {
        socket: WebSocket;
        isSocketOpen: boolean;
        currentContract: string;
    };
    dispatch: (Action: any) => void;
}

export type OrderBookAction = [number, number][];

export type EventMessage = {
    feed: string;
    product_id: string;
    bids: OrderBookAction;
    asks: OrderBookAction;
    numLevels?: number;
    event?: 'subscribed' | 'unsubscribed' | undefined;
};

export default function useOrderbook({
    state: {socket, isSocketOpen, currentContract},
    dispatch,
}: Props) {
    useEffect(() => {
        socket.onopen = () => {
            console.info('[Socket open] -> sending subscription message');
            socket.send(getContractMessage(currentContract));
        };
        socket.onmessage = (e: MessageEvent<EventMessage>) => {
            console.log('api data', e.data);
            //   feedWorker.postMessage(e.data);
            //   feedWorker.onmessage = (d) => {
            //     bookCache.current = d.data as OrderBookStateAction;
            //   };
        };
    }, [socket, currentContract]);

    return;
}
