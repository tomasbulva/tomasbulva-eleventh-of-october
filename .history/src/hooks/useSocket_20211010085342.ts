import {useEffect} from 'react';

interface Props {
    state: {
        socket: WebSocket;
        isSocketOpen: boolean;
    };
    dispatch: React.Dispatch<any>;
    onSocketMessage: () => void,
    onSocketOpen: () => void,
}

export default function useSocket({
    state: {socket, isSocketOpen},
    dispatch,
    onSocketMessage,
    onSocketOpen,
}: Props) {
    

    return {
        closeSocket,
        restartSocket,
    };
}
