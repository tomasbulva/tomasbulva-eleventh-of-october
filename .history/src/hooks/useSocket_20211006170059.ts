import {useCallback, useContext, useEffect} from 'react';

interface Props {
    state: {
        socket: WebSocket;
    };
    dispatch: (Action: any) => void;
}

export default function useSocket({state: {socket}, dispatch}: Props) {
    // dispatch({type: 'setSocket', payload: SOCKET});
    
    const closeSocket = useCallback(() => {
        socket.close();
        dispatch({type: 'closeSocket'});
    }, [socket]);

    const restartSocket = useCallback(() => {
        dispatch({type: 'setSocket', payload: new WebSocket(API)});
    }, []);

    useEffect(() => {
        socket.onerror = () => {
            dispatch({type: 'closeSocket'});
        };
        socket.onclose = () => {
            console.log('socket close callback');
            dispatch({type: 'closeSocket'});
        };
    }, [socket]);

    return {
        closeSocket,
        restartSocket,
    };
}
