import {useEffect} from 'react';

interface Props {
    state: {
        socket: WebSocket;
        isSocketOpen: boolean;
    };
    dispatch: React.Dispatch<any>;
    onSocketMessage: () => void,
    onSocketOpen: () => void,
}

export default function useSocket({
    state: {socket, isSocketOpen},
    dispatch,
    onSocketMessage,
    onSocketOpen,
}: Props) {
    useEffect(() => {
        if (!socket) {
            return;
        }
        socket.onerror = () => {
            dispatch({type: 'closeSocket'});
        };
        socket.onclose = () => {
            console.log('socket close callback');
            if (isSocketOpen) {
                dispatch({type: 'closeSocket'});
            }
        };
        socket.onopen = onSocketOpen;
        socket.onmessage = onSocketMessage;
    }, [socket]);

    return {
        closeSocket,
        restartSocket,
    };
}
