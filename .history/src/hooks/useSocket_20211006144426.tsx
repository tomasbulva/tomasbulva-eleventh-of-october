import { useContext } from 'react';
import { API } from '../utils/constans';
import { StateContext } from '../context/stateContext';
import { DispatchContext } from '../context/dispatchContext';

export const SOCKET = new WebSocket(API);

export default function useSocket({state, dispatch}) {
    const { isSocketOpen } = state;
    const { dispatch } = useContext(DispatchContext);
    const closeSocket = useCallback(() => {
        socket.close();
      }, [socket]);
    
      const restartSocket = useCallback(() => {
        setSocketState(1);
        const newSocket = new WebSocket(CF_WSS);
        setSocket(newSocket);
      }, []);
    
      // Whenever we create a new socket, attach default error and close handlers
      useEffect(() => {
        socket.onerror = () => {
          setSocketState(0);
        };
        socket.onclose = () => {
          console.log('socket close callback');
          setSocketState(0);
        };
      }, [socket]);
}