import { useEffect } from 'react';

interface Props {
    state: {
        isWindowFocused: boolean;
    };
    dispatch: (Action:any) => void;
}

export const useWindowFocus = ({state: { isWindowFocused }, dispatch }: Props) => {

  useEffect(() => {
    const onFocus = () => dispatch({type: 'setWindowFocused', payload: true});
    const onBlur = () => dispatch({type: 'setWindowFocused', payload: false});
    
    window.addEventListener('focus', onFocus);
    window.addEventListener('blur', onBlur);
    
    return () => {
      window.removeEventListener('focus', onFocus);
      window.removeEventListener('blur', onBlur);
    };
  }, []);

  return focused;
};