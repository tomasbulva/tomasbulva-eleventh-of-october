import {useEffect} from 'react';

interface Props {
    state: {
        isWindowFocused: boolean;
    };
    dispatch: (Action: any) => void;
    closeSocket: () => void; 
    restartSocket: () => void;
}

export default function useBrowser ({state: {isWindowFocused}, dispatch, closeSocket, restartSocket}: Props) {
    useEffect(() => {
        const onFocus = () => {
            dispatch({type: 'setWindowFocused', payload: true});
            restartSocket();
        }
        
        const onBlur = () => {
            dispatch({type: 'setWindowFocused', payload: false});
            closeSocket()
        }

        window.addEventListener('focus', onFocus);
        window.addEventListener('blur', onBlur);

        return () => {
            window.removeEventListener('focus', onFocus);
            window.removeEventListener('blur', onBlur);
        };
    }, []);

    return isWindowFocused;
};
