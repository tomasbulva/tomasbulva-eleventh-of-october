import {useEffect, useRef, useCallback} from 'react';
import {OrderbookMessage} from '../state/types';
import {getContractMessage, worker} from '../utils/api';

interface Props {
    state: {
        socket: WebSocket;
        isSocketOpen: boolean;
        currentContract: string;
        currentInterval: NodeJS.Timer;
    };
    dispatch: (Action: any) => void;
}

export type OrderBookAction = [number, number][];

export type EventMessage = {
    feed: string;
    product_id: string;
    bids: OrderBookAction;
    asks: OrderBookAction;
    numLevels?: number;
    event?: 'subscribed' | 'unsubscribed' | undefined;
};

export default function useOrderbook({
    state: {socket, isSocketOpen, currentContract, currentInterval},
    dispatch,
}: Props) {
    const buffer = useRef<OrderbookMessage | any>({});
    const isMounted = useRef<boolean>(false);

    const dumpBuffer = useCallback(() => {
        console.log('dumpBuffer', isSocketOpen);
        if (isSocketOpen && buffer.current) {
            console.log('buffer.current', buffer.current);
            dispatch({type: 'updateOrderbook', payload: buffer.current});
        }
        buffer.current = null;
    }, [isSocketOpen, dispatch]);

    if (currentInterval) {
        clearInterval(currentInterval);
    }
    if (isSocketOpen) {
        const interval = setInterval(dumpBuffer, 1000);
        dispatch({type: 'setInterval', payload: interval});
    }

    useEffect(() => {
        console.log('socket state update');
        socket.onopen = () => {
            console.info('Socket open');
            socket.send(getContractMessage(currentContract));
        };
        socket.onmessage = (e: MessageEvent<EventMessage>) => {
            //console.log('api data', e.data);
            worker.postMessage(e.data);
            worker.onmessage = (d) => {
                //console.log(d);
                buffer.current = d.data as OrderbookMessage;
            };
        };
    }, [socket, currentContract]);

    useEffect(() => {
        if (isMounted.current) {
            buffer.current = null;

            socket.send(getContractMessage(currentContract, false));
            socket.send(getContractMessage(currentContract));
        } else {
            isMounted.current = true;
        }
    }, [currentContract]);

    return;
}
