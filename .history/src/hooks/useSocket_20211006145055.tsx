import { useCallback, useContext, useEffect } from 'react';
import { API } from '../utils/constans';

export const SOCKET = new WebSocket(API);

export default function useSocket({{ isSocketOpen, socket }, dispatch}) {
    
    const closeSocket = useCallback(() => {
        socket.close();
        dispatch({type: 'closeSocket'})
    }, [socket]);
    
      const restartSocket = useCallback(() => {
        dispatch({type: 'setSocket', payload: new WebSocket(API)})
      }, []);
    
      // Whenever we create a new socket, attach default error and close handlers
      useEffect(() => {
        socket.onerror = () => {
          setSocketState(0);
        };
        socket.onclose = () => {
          console.log('socket close callback');
          setSocketState(0);
        };
      }, [socket]);

      return {
        closeSocket,

      }
}