import {useEffect} from 'react';

interface Props {
    state: {
        socket: WebSocket;
        isSocketOpen: boolean;
    };
    dispatch: React.Dispatch<any>;
    onSocketMessage: () => void,
    onSocketOpen: () => void,
}

export default function useSocket({
    state: {socket, isSocketOpen},
    dispatch,
}: Props) {
    const closeSocket = () => dispatch({type: 'closeSocket'});

    const restartSocket = () => dispatch({type: 'restartSocket'});

    useEffect(() => {
        if (!socket) {
            return;
        }
        socket.onerror = () => {
            dispatch({type: 'closeSocket'});
        };
        socket.onclose = () => {
            console.log('socket close callback');
            if (isSocketOpen) {
                dispatch({type: 'closeSocket'});
            }
        };
    }, [socket]);

    return {
        closeSocket,
        restartSocket,
    };
}
