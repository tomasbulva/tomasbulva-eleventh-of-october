import {useEffect, useRef, useCallback} from 'react';
import {OrderbookMessage} from '../state/types';
import {getContractMessage, worker} from '../utils/api';
import interval from '../utils/interval';

interface Props {
    state: {
        socket: WebSocket;
        currentContract: string;
        bufferDumpInterval: number;
    };
    dispatch: (Action: any) => void;
}

export type OrderBookAction = [number, number][];

export type EventMessage = {
    feed: string;
    product_id: string;
    bids: OrderBookAction;
    asks: OrderBookAction;
    numLevels?: number;
    event?: 'subscribed' | 'unsubscribed' | undefined;
};

export default function useOrderbook({
    state: {socket, currentContract, bufferDumpInterval},
    dispatch,
}: Props): void {
    const buffer = useRef<OrderbookMessage | any>({});

    const dumpBuffer = useCallback<() => void>(() => {
        if (buffer.current) {
            dispatch({type: 'updateOrderbook', payload: buffer.current});
        }
        buffer.current = null;
    }, [buffer]);

    useEffect(() => {
        if (!socket) {
            return;
        }
        if (socket.readyState === socket.CLOSED) {
            console.info('+ [socket] closed');
        }
        if (socket.readyState === socket.CLOSING) {
            console.info('+ [socket] closing/opening');
        }
        if (socket.readyState === socket.CONNECTING) {
            console.info('+ [socket] connecting');
        }
        if (socket.readyState === socket.OPEN) {
            console.info('+ [socket] open');
        }
    }, [socket, currentContract]);

    const socketOnError = useCallback(() => {
        console.info('- [socket] error');
        socket.send(getContractMessage(currentContract, false));
        dispatch({type: 'closeSocket'});
    }, [socket, dispatch]);

    const socketOnClose = useCallback(() => {
        interval.clearAll();
    }, [socket, dispatch]);

    const socketOnOpen = useCallback(() => {
        interval.clearAll();
        interval.create({callback: dumpBuffer, interval: bufferDumpInterval});
        socket.send(getContractMessage(currentContract));
    }, [socket, dispatch, currentContract]);

    const socketOnMessage = useCallback(
        (e: MessageEvent<EventMessage>) => {
            worker.postMessage(e.data);
            worker.onmessage = (d) => {
                buffer.current = d.data as OrderbookMessage;
            };
        },
        [socket, buffer, worker],
    );

    useEffect(() => {
        if (!socket) {
            return;
        }
        socket.onerror = socketOnError;
        socket.onclose = socketOnClose;
        socket.onopen = socketOnOpen;
        socket.onmessage = socketOnMessage;
    }, [socket]);
}
