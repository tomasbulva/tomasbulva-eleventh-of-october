import {useEffect, useCallback} from 'react';

interface Props {
    state: {
        isWindowFocused: boolean;
    };
    dispatch: (Action: any) => void;
}

export default function useBrowser({
    state: {isWindowFocused, socket},
    dispatch,
}: Props) {
    const onFocus = useCallback(() => {
        console.log('Browser Focus');
        dispatch({type: 'setWindowFocused', payload: true});
        dispatch({type: 'openSocket'});
    }, []);

    const onBlur = useCallback(() => {
        console.log('Browser Blur');
        dispatch({type: 'setWindowFocused', payload: false});
        dispatch({type: 'closeSocket'});
    }, []);

    const onClose = useCallback(() => {
        if (socket.readyState == WebSocket.OPEN) {
            socket.close();
        }
    }, []);

    useEffect(() => {
        window.addEventListener('focus', onFocus);
        window.addEventListener('blur', onBlur);
        window.addEventListener('unload', onClose);

        return () => {
            window.removeEventListener('focus', onFocus);
            window.removeEventListener('blur', onBlur);
            window.removeEventListener('unload', onClose);
        };
    }, []);

    return isWindowFocused;
}
