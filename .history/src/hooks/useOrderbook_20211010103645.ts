import {useEffect, useRef, useCallback} from 'react';
import {OrderbookMessage} from '../state/types';
import {getContractMessage, worker} from '../utils/api';

interface Props {
    state: {
        socket: WebSocket;
        isSocketOpen: boolean;
        currentContract: string;
        currentInterval: NodeJS.Timer;
        bufferDumpInterval: number;
    };
    dispatch: (Action: any) => void;
}

export type OrderBookAction = [number, number][];

export type EventMessage = {
    feed: string;
    product_id: string;
    bids: OrderBookAction;
    asks: OrderBookAction;
    numLevels?: number;
    event?: 'subscribed' | 'unsubscribed' | undefined;
};

export default function useOrderbook({
    state: {
        socket,
        isSocketOpen,
        currentContract,
        currentInterval,
        bufferDumpInterval,
    },
    dispatch,
}: Props): void {
    const buffer = useRef<OrderbookMessage | any>({});

    const dumpBuffer = useCallback(() => {
        console.log('dumpBuffer isSocketOpen', isSocketOpen);
        if (isSocketOpen && buffer.current) {
            //console.log('buffer.current', buffer.current);
            dispatch({type: 'updateOrderbook', payload: buffer.current});
        }
        buffer.current = null;
    }, [isSocketOpen, dispatch]);

    useEffect(() => {
        if (!socket) {
            return;
        }
        if (socket.readyState === socket.CLOSED) {
            console.info('+ [socket] closed');
        }
        if (socket.readyState === socket.CLOSING) {
            console.info('+ [socket] closing/opening');
        }
        if (socket.readyState === socket.CONNECTING) {
            console.info('+ [socket] connecting');
        }
        if (socket.readyState === socket.OPEN) {
            console.info('+ [socket] open');
        }

        // console.log('socket.readyState', socket.readyState);
        // console.log('socket.CLOSED', socket.CLOSED);
        // console.log('socket.CLOSING', socket.CLOSING);
        // console.log('socket.CONNECTING', socket.CONNECTING);
        // console.log('socket.OPEN', socket.OPEN);
    }, [socket, isSocketOpen, currentInterval, currentContract]);

    const socketOnError = useCallback(() => {
        console.log('- [socket] error');
        dispatch({type: 'closeSocket'});
    }, [dispatch]);

    const socketOnClose = useCallback(() => {
        console.log('- [socket] closed');
        dispatch({type: 'setSocketStateClosed'});
        clearInterval(currentInterval);
    }, [dispatch]);

    const socketOnOpen = useCallback(() => {
        console.log('- [socket] opened', isSocketOpen);
        dispatch({type: 'setSocketStateOpen'});
        dispatch({
            type: 'setIntervalRef',
            payload: setInterval(dumpBuffer, bufferDumpInterval),
        });
        socket.send(getContractMessage(currentContract));
    }, [socket, dispatch, isSocketOpen, currentContract]);

    const socketOnMessage = useCallback(
        (e: MessageEvent<EventMessage>) => {
            console.log('- [socket] message received', isSocketOpen);
            worker.postMessage(e.data);
            worker.onmessage = (d) => {
                buffer.current = d.data as OrderbookMessage;
            };
        },
        [isSocketOpen],
    );

    useEffect(() => {
        if (!socket) {
            return;
        }
        socket.onerror = socketOnError;
        socket.onclose = socketOnClose;
        socket.onopen = socketOnOpen;
        socket.onmessage = socketOnMessage;
    }, [socket]);

    // useEffect(() => {
    //     if (isMounted.current) {
    //         buffer.current = null;

    //         socket.send(getContractMessage(currentContract, false));
    //         socket.send(getContractMessage(currentContract));
    //     } else {
    //         isMounted.current = true;
    //     }
    // }, [currentContract]);
}
