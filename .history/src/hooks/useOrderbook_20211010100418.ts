import {useEffect, useRef, useCallback} from 'react';
import {OrderbookMessage} from '../state/types';
import {getContractMessage, worker} from '../utils/api';

interface Props {
    state: {
        socket: WebSocket;
        isSocketOpen: boolean;
        currentContract: string;
        currentInterval: NodeJS.Timer;
        bufferDumpInterval: number;
    };
    dispatch: (Action: any) => void;
}

export type OrderBookAction = [number, number][];

export type EventMessage = {
    feed: string;
    product_id: string;
    bids: OrderBookAction;
    asks: OrderBookAction;
    numLevels?: number;
    event?: 'subscribed' | 'unsubscribed' | undefined;
};

export default function useOrderbook({
    state: {
        socket,
        isSocketOpen,
        currentContract,
        currentInterval,
        bufferDumpInterval,
    },
    dispatch,
}: Props): void {
    const buffer = useRef<OrderbookMessage | any>({});

    const dumpBuffer = useCallback(() => {
        console.log('dumpBuffer isSocketOpen', isSocketOpen);
        if (isSocketOpen && buffer.current) {
            //console.log('buffer.current', buffer.current);
            dispatch({type: 'updateOrderbook', payload: buffer.current});
        }
        buffer.current = null;
    }, [isSocketOpen, dispatch]);

    useEffect(() => {
        if (isSocketOpen && !currentInterval) {
            console.log('isSocketOpen', isSocketOpen);
            console.log('currentInterval', currentInterval);

            dispatch({
                type: 'setIntervalRef',
                payload: setInterval(dumpBuffer, bufferDumpInterval),
            });
            return;
        }
        if (!isSocketOpen && currentInterval) {
            clearInterval(currentInterval);
        }
    }, [isSocketOpen, currentInterval]);

    useEffect(() => {
        if (!socket) {
            return;
        }
        if (socket.CLOSED) {
            console.info('+ [socket] closed');
            dispatch({type: 'setSocketStateClosed'});
        }
        if (socket.CLOSING) {
            console.info('+ [socket] closing/opening');
        }
        if (socket.CONNECTING) {
            console.info('+ [socket] connecting');
        }
        if (socket.OPEN) {
            console.info('+ [socket] open');
            dispatch({type: 'setSocketStateOpen'});
        }
    }, [socket, isSocketOpen, currentInterval, currentContract]);

    useEffect(() => {
        if (!socket) {
            return;
        }
        socket.onerror = () => {
            dispatch({type: 'closeSocket'});
        };
        socket.onclose = () => {
            console.log('socket close callback');
            // if (isSocketOpen) {
            //     dispatch({type: 'closeSocket'});
            // }
        };
        socket.onopen = () => {
            console.log('---- Socket open isSocketOpen', isSocketOpen);
            socket.send(getContractMessage(currentContract));
        };
        socket.onmessage = (e: MessageEvent<EventMessage>) => {
            // console.log('api data', e.data);
            worker.postMessage(e.data);
            worker.onmessage = (d) => {
                //console.log(d);
                buffer.current = d.data as OrderbookMessage;
            };
        };
    }, [socket, isSocketOpen]);

    // useEffect(() => {
    //     if (isMounted.current) {
    //         buffer.current = null;

    //         socket.send(getContractMessage(currentContract, false));
    //         socket.send(getContractMessage(currentContract));
    //     } else {
    //         isMounted.current = true;
    //     }
    // }, [currentContract]);
}
