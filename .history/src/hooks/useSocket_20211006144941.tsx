import { useCallback, useContext } from 'react';
import { API } from '../utils/constans';
import { StateContext } from '../context/stateContext';
import { DispatchContext } from '../context/dispatchContext';

export const SOCKET = new WebSocket(API);

export default function useSocket({state, dispatch}) {
    const { isSocketOpen, socket } = state;
    
    const closeSocket = useCallback(() => {
        socket.close();
        dispatch({type: 'closeSocket'})
    }, [socket]);
    
      const restartSocket = useCallback(() => {

        const newSocket = ;

        dispatch({type: 'setSocket', payload: new WebSocket(API)})
      }, []);
    
      // Whenever we create a new socket, attach default error and close handlers
      useEffect(() => {
        socket.onerror = () => {
          setSocketState(0);
        };
        socket.onclose = () => {
          console.log('socket close callback');
          setSocketState(0);
        };
      }, [socket]);

      return {
        closeSocket,

      }
}