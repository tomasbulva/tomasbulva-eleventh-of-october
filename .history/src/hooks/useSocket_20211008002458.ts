import {useCallback, useEffect} from 'react';
import {API} from '../utils/constans';

interface Props {
    state: {
        socket: WebSocket;
    };
    dispatch: (Action: any) => void;
}

export default function useSocket({state: {socket}, dispatch}: Props) {
    const closeSocket = useCallback(() => {
        socket.close();
        dispatch({type: 'closeSocket'});
    }, [socket]);

    const restartSocket = useCallback(() => {
        console.log('restarting socket');
        closeSocket();
        const newSocket = new WebSocket(API);
        dispatch({type: 'setSocket', payload: newSocket});
    }, []);

    useEffect(() => {
        if (!socket) {
            return;
        }
        socket.onerror = () => {
            dispatch({type: 'closeSocket'});
        };
        socket.onclose = () => {
            console.log('socket close callback');
            dispatch({type: 'closeSocket'});
        };
    }, [socket]);

    return {
        closeSocket,
        restartSocket,
    };
}
