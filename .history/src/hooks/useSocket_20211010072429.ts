import {useCallback, useEffect} from 'react';
import {API} from '../utils/constans';

interface Props {
    state: {
        socket: WebSocket;
        isSocketOpen: boolean;
    };
    dispatch: (Action: any) => void;
}

export default function useSocket({state: {socket, isSocketOpen}, dispatch}: Props) {
    const closeSocket = useCallback(() => {
        console.log('closeSocket');
        socket.close();
        dispatch({type: 'closeSocket'});
    }, [socket]);

    const restartSocket = useCallback(() => {
        console.log('restarting socket');
        closeSocket();
        const newSocket = new WebSocket(API);
        dispatch({type: 'setSocket', payload: newSocket});
    }, []);

    useEffect(() => {
        if (!socket) {
            return;
        }
        socket.onerror = () => {
            dispatch({type: 'closeSocket'});
        };
        socket.onclose = () => {
            console.log('socket close callback');
            if (isSocketOpen) {
                dispatch({type: 'closeSocket'});
            }
        };
    }, [socket]);

    useEffect(() => {
        if (socket?.OPEN) {
            dispatch({type: 'setSocketOpen'});
        }
    });

    return {
        closeSocket,
        restartSocket,
    };
}
