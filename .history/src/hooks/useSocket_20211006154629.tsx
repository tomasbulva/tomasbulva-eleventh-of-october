import { useCallback, useContext, useEffect } from 'react';
import { API } from '../utils/constans';

export const SOCKET = new WebSocket(API);

interface Props {
    state: {
        isSocketOpen: boolean;
        socket: WebSocket;
    };
    dispatch: (Action:any) => void;
}

export default function useSocket({{ isSocketOpen, socket }, dispatch }) {
    
    const closeSocket = useCallback(() => {
        socket.close();
        dispatch({type: 'closeSocket'})
    }, [socket]);
    
    const restartSocket = useCallback(() => {
        dispatch({type: 'setSocket', payload: new WebSocket(API)})
    }, []);

    // Whenever we create a new socket, attach default error and close handlers
    useEffect(() => {
    socket.onerror = () => {
        dispatch({type: 'closeSocket'})
    };
    socket.onclose = () => {
        console.log('socket close callback');
        dispatch({type: 'closeSocket'})
    };
    }, [socket]);

    return {
        closeSocket,
    }
}