import {useEffect, useCallback} from 'react';

interface Props {
    state: {
        isWindowFocused: boolean;
    };
    dispatch: (Action: any) => void;
}

export default function useBrowser({
    state: {isWindowFocused},
    dispatch,
}: Props) {
    const onFocus = useCallback(() => {
        console.log('Browser Focus');
        dispatch({type: 'setWindowFocused', payload: true});
        dispatch({type: 'closeSocket'});
    }, []);

    const onBlur = () => {
        console.log('Browser Blur');
        dispatch({type: 'setWindowFocused', payload: false});
        closeSocket();
    };

    useEffect(() => {
        window.addEventListener('focus', onFocus);
        window.addEventListener('blur', onBlur);

        return () => {
            window.removeEventListener('focus', onFocus);
            window.removeEventListener('blur', onBlur);
        };
    }, []);

    return isWindowFocused;
}
