import { useEffect } from 'react';

interface Props {
    state: {
        isWindowFocused: boolean;
    };
    dispatch: (Action:any) => void;
}

export const useWindowFocus = ({state: { isWindowFocused }, dispatch }: Props) => {

  const [focused, setFocused] = useState<boolean>(true);

  useEffect(() => {
    const onFocus = () => setFocused(true);
    const onBlur = () => setFocused(false);
    window.addEventListener('focus', onFocus);
    window.addEventListener('blur', onBlur);
    return () => {
      window.removeEventListener('focus', onFocus);
      window.removeEventListener('blur', onBlur);
    };
  }, []);

  return focused;
};