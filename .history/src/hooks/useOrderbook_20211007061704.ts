import { useEffect, useRef, useCallback } from 'react';
import { OrderbookMessage } from '../utils/worker';
import { getContractMessage, getUnsubscribeMessage, worker } from '../utils/api';

interface Props {
    state: {
        socket: WebSocket;
        isSocketOpen: boolean;
        currentContract: string;
    };
    dispatch: (Action: any) => void;
}

export type OrderBookAction = [number, number][];

export type EventMessage = {
    feed: string;
    product_id: string;
    bids: OrderBookAction;
    asks: OrderBookAction;
    numLevels?: number;
    event?: 'subscribed' | 'unsubscribed' | undefined;
};

export default function useOrderbook({
    state: {socket, isSocketOpen, currentContract},
    dispatch,
}: Props) {
    const buffer = useRef<OrderbookMessage | null>(null);
    const isMounted = useRef<boolean>(false);
    
    
    const flushBookCache = useCallback(() => {
        if (isSocketOpen && buffer.current) {
            dispatch({type: 'updateOrderbook', payload: buffer.current});
        }
        buffer.current = null;
      }, [isSocketOpen, dispatch]);
    
      setInterval(flushBookCache, 500);
    
    
    useEffect(() => {
        socket.onopen = () => {
            console.info('[Socket open] -> sending subscription message');
            socket.send(getContractMessage(currentContract));
        };
        socket.onmessage = (e: MessageEvent<EventMessage>) => {
            console.log('api data', e.data);
            worker.postMessage(e.data);
            worker.onmessage = (d) => {
                buffer.current = d.data as OrderbookMessage;
            };
        };
    }, [socket, currentContract]);

    useEffect(() => {
        if (isMounted.current) {
            buffer.current = null;

            socket.send(unsubMessage);
            socket.send(getContractMessage(currentContract));
        } else {
            isMounted.current = true;
        }
    }, [currentContract, subMessage]);

    return;
}

