import {useEffect, useRef, useCallback} from 'react';
import {OrderbookMessage} from '../state/types';
import {getContractMessage, worker} from '../utils/api';

interface Props {
    state: {
        socket: WebSocket;
        currentContract: string;
        bufferDumpInterval: number;
    };
    dispatch: (Action: any) => void;
}

export type OrderBookAction = [number, number][];

export type EventMessage = {
    feed: string;
    product_id: string;
    bids: OrderBookAction;
    asks: OrderBookAction;
    numLevels?: number;
    event?: 'subscribed' | 'unsubscribed' | undefined;
};

export default function useOrderbook({
    state: {socket, currentContract, bufferDumpInterval},
    dispatch,
}: Props): void {
    const buffer = useRef<OrderbookMessage | any>({});
    let intervalID: NodeJS.Timer | null = null;
    let isSocketOpen: boolean = false;

    const dumpBuffer = useCallback(() => {
        if (buffer.current) {
            dispatch({type: 'updateOrderbook', payload: buffer.current});
        }
        buffer.current = null;
    }, [buffer]);

    useEffect(() => {
        if (!socket) {
            return;
        }
        if (socket.readyState === socket.CLOSED) {
            console.info('+ [socket] closed');
        }
        if (socket.readyState === socket.CLOSING) {
            console.info('+ [socket] closing/opening');
        }
        if (socket.readyState === socket.CONNECTING) {
            console.info('+ [socket] connecting');
        }
        if (socket.readyState === socket.OPEN) {
            console.info('+ [socket] open');
        }
    }, [socket, intervalID, currentContract]);

    const socketOnError = useCallback(() => {
        console.log('- [socket] error');
        dispatch({type: 'closeSocket'});
    }, [socket, dispatch]);

    const socketOnClose = useCallback(() => {
        console.log('- [socket] closed');
        isSocketOpen = false;
        clearInterval(intervalID);
    }, [socket, dispatch]);

    const socketOnOpen = useCallback(() => {
        console.log('- [socket] opened');
        isSocketOpen = true;
        intervalID = setInterval(dumpBuffer, bufferDumpInterval);
        socket.send(getContractMessage(currentContract));
    }, [socket, dispatch, currentContract]);

    const socketOnMessage = useCallback(
        (e: MessageEvent<EventMessage>) => {
            // console.log('- [socket] message received');
            worker.postMessage(e.data);
            worker.onmessage = (d) => {
                buffer.current = d.data as OrderbookMessage;
            };
        },
        [socket, buffer, worker],
    );

    useEffect(() => {
        if (!socket) {
            return;
        }
        socket.onerror = socketOnError;
        socket.onclose = socketOnClose;
        socket.onopen = socketOnOpen;
        socket.onmessage = socketOnMessage;
    }, [socket]);
}
