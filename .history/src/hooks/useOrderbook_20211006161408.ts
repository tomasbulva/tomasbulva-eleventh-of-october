interface Props {
    state: {
        socket: WebSocket;
        isSocketOpen: boolean;
    };
    dispatch: (Action:any) => void;
}


export default function useOrderbook({}) {
    
    
    useEffect(() => {
        socket.onopen = () => {
          console.info('[Socket open] -> sending subscription message');
          socket.send(subMessage);
        };
        socket.onmessage = (e: MessageEvent<EventMessage>) => {
          feedWorker.postMessage(e.data);
          feedWorker.onmessage = (d) => {
            bookCache.current = d.data as OrderBookStateAction;
          };
        };
      }, [socket, subMessage]);
    
    return 
}