import {ResultLine} from './componenets/resultLine';
import {HeaderLine} from './componenets/headerLine';
import React, {useState} from 'react';
import ToggleSwitch from './componenets/toggleSwitch/toggleSwitch';
import './styles.scss';

export const OrderBook = () => {
    const [isToggled, setIsToggled] = useState(false);

    return (
        <main>
            <section className="orderbook">
                <aside className="green">
                    <header>
                        Order Book (ETH-USD)
                    </header>
                    <HeaderLine />
                    <ResultLine />
                </aside>
                <aside className="red">
                    <header>
                        Spread 17.0 (0.05%)
                    </header>
                    <HeaderLine />
                    <ResultLine />
                </aside>
            </section>
            <footer>
                <ToggleSwitch
                    isToggled={isToggled}
                    setIsToggled={setIsToggled}
                />
            </footer>
        </main>
    );
};
