import React, {useReducer, useState} from 'react';

import RedSide from './componenets/redSide';
import GreenSide from './componenets/greenSide';
import ToggleSwitch from './componenets/toggleSwitch';

import {StateContext} from './context/stateContext';
import {DispatchContext} from './context/dispatchContext';

import {defState, reducer} from './state';

import './styles.scss';
import useBrowser from './hooks/useBrowser';
import useSocket from './hooks/useSocket';
import useOrderbook from './hooks/useOrderbook';

export default function OrderBook() {
    const [state, dispatch] = useReducer(reducer, defState);
    const [isToggled, setIsToggled] = useState(false);

    const isWindowFocused = useBrowser({state, dispatch});
    const { closeSocket, restartSocket } = useSocket({state, dispatch});
    // const orderbook = useOrderbook({state, dispatch});

    // <DispatchContext.Provider value={dispatch}>
    // <StateContext.Provider value={state}>
    // </StateContext.Provider>
    // </DispatchContext.Provider>

    return (
        <main>
            <section className="orderbook">
                <GreenSide />
                <RedSide />
            </section>
            <footer>
                <ToggleSwitch
                    isToggled={isToggled}
                    setIsToggled={setIsToggled}
                />
            </footer>
        </main>
    );
}
