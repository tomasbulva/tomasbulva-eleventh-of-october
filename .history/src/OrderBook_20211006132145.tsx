import { RedSide } from './componenets/redSide';
import { GreenSide } from './componenets/greenSide';
import React, { useState } from 'react';
import ToggleSwitch from './componenets/toggleSwitch';
import './styles.scss';

export const OrderBook = () => {
    const [isToggled, setIsToggled] = useState(false);

    return (
        <main>
            <section className="orderbook">
                <GreenSide />
                <RedSide />
            </section>
            <footer>
                <ToggleSwitch
                    isToggled={isToggled}
                    setIsToggled={setIsToggled}
                />
            </footer>
        </main>
    );
};
