// import React, {createContext} from 'react';
import {CONTRACT} from '../utils/constans';
import {API} from '../utils/constans';

export interface GlobalStateIntf {
    isWindowFocused: boolean;
    currentContract: string;
    socket: WebSocket;
    isSocketOpen: boolean;
}

export const SOCKET = new WebSocket(API);

export const defState = {
    isWindowFocused: true,
    currentContract: CONTRACT['XBT-USD'],
    socket: SOCKET,
    isSocketOpen: false,
};

export const reducer = (state, action) => {
    console.log('reducer state', state);
    console.log('reducer action', action);
    switch (action.type) {
        case 'setSocket':
            return {...state, socket: action.payload, isSocketOpen: true};
        case 'closeSocket':
            return {...state, isSocketOpen: false};
        case 'setWindowFocused':
            return {...state, isWindowFocused: action.payload};
        case 'updateOrderbook':
            return { ...state, time: state.time + 1 };
        default:
            throw new Error('Bad action type!');
    }
};
