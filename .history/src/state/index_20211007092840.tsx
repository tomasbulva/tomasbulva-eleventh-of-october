// import React, {createContext} from 'react';
import {CONTRACT} from '../utils/constans';
import {API} from '../utils/constans';

export type OrderbookRow = [number, number, number];
export type OrderbookRows = OrderbookRow[];
export type OrderbookCalcRows = [number, number][];
export type Spread = {value: number; percent: number};

export type OrderbookMessage = {
    asks: OrderbookRows;
    bids: OrderbookRows;
    spread: Spread;
    highestTotal: number;
};

export type EventMessage = {
    feed: string;
    product_id: string;
    bids: OrderbookCalcRows;
    asks: OrderbookCalcRows;
    numLevels?: number;
    event?: 'subscribed' | 'unsubscribed' | undefined;
};

export interface GlobalStateIntf {
    isWindowFocused: boolean;
    currentContract: string;
    socket: WebSocket;
    isSocketOpen: boolean;
    bidsandasks: OrderbookMessage;
}

export const SOCKET = new WebSocket(API);

export const defState = {
    isWindowFocused: true,
    currentContract: CONTRACT['XBT-USD'],
    socket: SOCKET,
    isSocketOpen: false,
    bidsandasks: null,
};

export const reducer = (state, action) => {
    console.log('reducer state', state);
    console.log('reducer action', action);
    switch (action.type) {
        case 'setSocket':
            return {...state, socket: action.payload, isSocketOpen: true};
        case 'closeSocket':
            return {...state, isSocketOpen: false};
        case 'setWindowFocused':
            return {...state, isWindowFocused: action.payload};
        case 'updateOrderbook':
            return { ...state, bidsandasks: action.payload};
        default:
            throw new Error('Bad action type!');
    }
};
