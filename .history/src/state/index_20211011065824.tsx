// import React, {createContext} from 'react';
import {CONTRACT} from '../utils/constans';
import {API} from '../utils/constans';
import {OrderbookMessage} from './types';

export interface GlobalStateIntf {
    isWindowFocused: boolean;
    currentContract: string;
    socket: WebSocket | null;
    orderbook: OrderbookMessage;
    bufferDumpInterval: number;
}

export const defState = {
    isWindowFocused: true,
    currentContract: CONTRACT['XBT-USD'],
    socket: null,
    orderbook: {},
    bufferDumpInterval: 1000,
};

export const reducer = (state, action) => {
    switch (action.type) {
        case 'openSocket':
            return {...state, socket: new WebSocket(API)};
        case 'closeSocket':
            state.socket.close(1000, 'going away');
            return state;
        case 'setWindowFocused':
            return {...state, isWindowFocused: action.payload};
        case 'updateOrderbook':
            return {...state, orderbook: action.payload};
        case 'setIntervalSize':
            return {...state, bufferDumpInterval: action.payload};
        case 'clearOrderBook':
            return {...state, orderbook: {}};
        case 'contractToggle':
            // only switching between two contracts.
            // so if statement is enaugh
            if (CONTRACT['XBT-USD']) {
                console.log(CONTRACT['ETH-USD']);
                return {
                    ...state,
                    currentContract: CONTRACT['ETH-USD'],
                };
            }
            return {...state, currentContract: CONTRACT['XBT-USD']};
        default:
            throw new Error(`Bad action type! - ${JSON.stringify(action)}`);
    }
};
