import React, {createContext} from 'react';

interface GlobalState {
    isWindowFocused: boolean,
}

const StateContext = createContext<Partial<GlobalState>>({});
const DispatchContext = createContext(null);

const initialState = {
    isWindowFocused: true,
    
  };