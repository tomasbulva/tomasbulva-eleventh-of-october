// import React, {createContext} from 'react';
import {CONTRACT} from '../utils/constans';
import {API} from '../utils/constans';
import {OrderbookMessage} from './types';

export interface GlobalStateIntf {
    isWindowFocused: boolean;
    currentContract: string;
    socket: WebSocket | null;
    isSocketOpen: boolean;
    orderbook: OrderbookMessage;
}

export const defState = {
    isWindowFocused: true,
    currentContract: CONTRACT['XBT-USD'],
    socket: null,
    isSocketOpen: false,
    orderbook: {},
};

export const reducer = (state, action) => {
    switch (action.type) {
        case 'openSocket':
            return {...state, socket: new WebSocket(API)};
        case 'closeSocket':
            state.socket.close();
            return {...state, socket: null};
        case 'setSocketStateOpen': 
            return {...state, isSocketOpen: true};
        case 'setSocketStateClosed': 
            return {...state, isSocketOpen: false};
        case 'setWindowFocused':
            return {...state, isWindowFocused: action.payload};
        case 'updateOrderbook':
            return {...state, orderbook: action.payload};
        case 'setInterval':
            return {...state, currentInterval: action.payload};
        default:
            throw new Error('Bad action type!');
    }
};
