import React, {createContext} from 'react';
import { CONTRACT } from '../utils/constans';

export interface GlobalState {
    isWindowFocused: boolean;
    currentContract: string;
    socket: WebSocket;
    isSocketOpen: boolean;
}

const StateContext = createContext<Partial<GlobalState>>({});
const DispatchContext = createContext(null);

const initialState = {
    isWindowFocused: true,
    currentContract: CONTRACT['XBT-USD'],
    socket: null,
    isSocketOpen: false,

  };