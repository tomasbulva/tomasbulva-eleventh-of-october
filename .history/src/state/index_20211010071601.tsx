// import React, {createContext} from 'react';
import {CONTRACT} from '../utils/constans';
import {API} from '../utils/constans';
import {OrderbookMessage} from './types';

export interface GlobalStateIntf {
    isWindowFocused: boolean;
    currentContract: string;
    socket: WebSocket | null;
    isSocketOpen: boolean;
    orderbook: OrderbookMessage;
}

export const SOCKET = new WebSocket(API);

export const defState = {
    isWindowFocused: true,
    currentContract: CONTRACT['XBT-USD'],
    socket: SOCKET,
    isSocketOpen: false,
    orderbook: {},
};

export const reducer = (state, action) => {
    console.log('reducer state', state);
    console.log('reducer action', action);
    switch (action.type) {
        case 'setSocket':
            return {...state, socket: action.payload};
        case 'setSocketOpen':
            return {...state, isSocketOpen: true};
        case 'closeSocket':
            return {...state, isSocketOpen: false, socket: null};
        case 'setWindowFocused':
            return {...state, isWindowFocused: action.payload};
        case 'updateOrderbook':
            return {...state, orderbook: action.payload};
        case 'setInterval':
            return {...state, currentInterval: action.payload};
        default:
            throw new Error('Bad action type!');
    }
};
