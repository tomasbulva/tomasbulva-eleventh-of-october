// import React, {createContext} from 'react';
import { CONTRACT } from '../utils/constans';

export interface GlobalStateIntf {
    isWindowFocused: boolean;
    currentContract: string;
    socket: WebSocket;
    isSocketOpen: boolean;
}


const initialState = {
    isWindowFocused: true,
    currentContract: CONTRACT['XBT-USD'],
    socket: null,
    isSocketOpen: false,

  };