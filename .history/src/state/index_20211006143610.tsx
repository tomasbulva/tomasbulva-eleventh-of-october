// import React, {createContext} from 'react';
import { CONTRACT } from '../utils/constans';

export interface GlobalStateIntf {
    isWindowFocused: boolean;
    currentContract: string;
    socket: WebSocket;
    isSocketOpen: boolean;
}

export const defState = {
    isWindowFocused: true,
    currentContract: CONTRACT['XBT-USD'],
    socket: null,
    isSocketOpen: false,

};

export const reducer = (state, action) => {
    switch (action.type) {
        case 'setSocket':
            return { ...state, socket: action.payload };
        case 'stop':
            return { ...state, isRunning: false };
        case 'reset':
            return { isRunning: false, time: 0 };
        case 'tick':
            return { ...state, time: state.time + 1 };
        default:
            throw new Error('Bad action type!');
    }
}