import React, {createContext} from 'react';

interface GlobalState {
    isWindowFocused: boolean,
}

const StateContext = createContext({}: GlobalState);
const DispatchContext = createContext(null);

const initialState = {
    isWindowFocused: true,


  };