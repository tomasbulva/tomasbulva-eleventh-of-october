import { createContext } from "react";
export const StateContext = createContext<Partial<GlobalState>>({});