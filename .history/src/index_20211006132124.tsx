import React from 'react';
import ReactDOM from 'react-dom';
import { OrderBook } from './OrderBook';

ReactDOM.render(
    <React.StrictMode>
        <OrderBook />
    </React.StrictMode>,
    document.getElementById('root'),
);
