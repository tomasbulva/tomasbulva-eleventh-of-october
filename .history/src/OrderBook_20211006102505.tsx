import React, {useState} from 'react';
import ToggleSwitch from './componenets/toggleSwitch/toggleSwitch';
import './styles.scss';

export const OrderBook = () => {
    const [isToggled, setIsToggled] = useState(false);

    return (
        <div className="page">
            <div className="orderbook">
                <section>
                    <header>
                        <span>Order Book</span>
                        <span>Spread</span>
                        <ToggleSwitch
                            isToggled={isToggled}
                            setIsToggled={setIsToggled}
                        />
                    </header>
                    <div className="board">
                        <div className="green-side">
                            <div className="header-line">
                                <div className="total">TOTAL</div>
                                <div className="size">SIZE</div>
                                <div className="total">PRICE</div>
                            </div>
                            <div className="order-line">
                                <div className="total">1,200</div>
                                <div className="size">1,200</div>
                                <div className="total">34,062.50</div>
                                <div className="background" />
                            </div>
                        </div>
                        <div className="red-side">
                        
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
};
