import React, {useState} from 'react';
import ToggleSwitch from './componenets/toggleSwitch/toggleSwitch';
import './styles.scss';

export const OrderBook = () => {
    const [isToggled, setIsToggled] = useState(false);

    return (
        <div className="page">
            <div className="orderbook">
                <section>
                    <header>
                        <span>Order Book</span>
                        <span>Spread</span>
                        <ToggleSwitch
                            isToggled={isToggled}
                            setIsToggled={setIsToggled}
                        />
                    </header>
                    <div className="board">
                        <div className="green-side">
                            <div className="header-line">
                                <div className="total">TOTAL</div>
                                <div className="size">SIZE</div>
                                <div className="total">PRICE</div>
                            </div>
                            <div className="order-line">
                                <div className="total"></div>
                                <div className="size"></div>
                                <div className="total"></div>
                            </div>
                        </div>
                        <div className="red-side">
                            <div className="header"><h5>TOTAL</h5><h5>SIZE</h5><h5>PRICE</h5></div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
};
