import React, { useState } from 'react';
import ToggleSwitch from './componenets/toggleSwitch/toggleSwitch';
import './styles.scss';

export const OrderBook = () => (
    const [isToggled, setIsToggled] = useState(false);
    <div className="page">
        <div className="orderbook">
            <section>
                <header><span>Order Book</span><span>Spread</span><ToggleSwitch isToggled={} setIsToggled /></header>
            </section>
        </div>
    </div>
);
