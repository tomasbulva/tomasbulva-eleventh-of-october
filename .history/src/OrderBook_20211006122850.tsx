import { OrderbookHeaderRed } from './componenets/OrderbookHeaderRed';
import { OrderbookHeaderGreen } from './componenets/orderbookHeaderGreen';
import {ResultLine} from './componenets/resultLine';
import {HeaderLine} from './componenets/headerLine';
import React, {useState} from 'react';
import ToggleSwitch from './componenets/toggleSwitch/toggleSwitch';
import './styles.scss';

export const OrderBook = () => {
    const [isToggled, setIsToggled] = useState(false);

    return (
        <main>
            <section className="orderbook">
                <aside className="green">
                    <OrderbookHeaderGreen />
                    <HeaderLine />
                    <ResultLine />
                </aside>
                <aside className="red">
                    <OrderbookHeaderRed />
                    <HeaderLine />
                    <ResultLine />
                </aside>
            </section>
            <footer>
                <ToggleSwitch
                    isToggled={isToggled}
                    setIsToggled={setIsToggled}
                />
            </footer>
        </main>
    );
};
