import React, {useState} from 'react';
import ToggleSwitch from './componenets/toggleSwitch/toggleSwitch';
import './styles.scss';

export const OrderBook = () => {
    const [isToggled, setIsToggled] = useState(false);

    return (
        <main>
            <div className="orderbook">
                <section>
                    <div className="board">
                        <aside className="green">
                            <header>
                                <span>Order Book</span>
                            </header>
                            <div className="header-line">
                                <div className="total">TOTAL</div>
                                <div className="size">SIZE</div>
                                <div className="total">PRICE</div>
                            </div>
                            <div className="order-line">
                                <div className="total">1,200</div>
                                <div className="size">1,200</div>
                                <div className="total">34,062.50</div>
                                <div className="background" />
                            </div>
                        </aside>
                        <aside className="red">
                            <header>
                                <span>Spread 17.0 (0.05%)</span>
                            </header>
                            <div className="header-line">
                                <div className="total">TOTAL</div>
                                <div className="size">SIZE</div>
                                <div className="total">PRICE</div>
                            </div>
                            <div className="order-line">
                                <div className="total">1,200</div>
                                <div className="size">1,200</div>
                                <div className="total">34,062.50</div>
                                <div className="background" />
                            </div>
                        </aside>
                    </div>
                    <footer><ToggleSwitch isToggled={isToggled} setIsToggled={setIsToggled} /></footer>
                </section>
            </div>
        </main>
    );
};
